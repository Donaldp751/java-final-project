package window;

public class ClassPair {
    private String Name;
    private int Priority;
    public ClassPair(String className, int priority){
    Name = className;
    Priority = priority;
    }

    public ClassPair SetPriority(int priority){
        Priority = priority;
        return this;
    }
    public ClassPair SetName(String name){
        Name = name;
        return this;
    }

    public String getName(){
        return Name;
    }
    public int getPriority(){
        return Priority;
    }

     @Override
    public String toString(){
        return Name + " : " + Integer.toString(Priority);
     }
}
