package window;

import DCTScheduleBuilder.ClassEntry;

import java.util.ArrayList;
import java.util.List;

public class ScheduleCreator {
    private List<ClassEntry> ClassPool = null;
    private List<ClassPair> ClassPairs = null;

    public ScheduleCreator(List<ClassPair> desiredClasses, List<ClassEntry> classPool){
        ClassPairs = desiredClasses;
        ClassPool = classPool;
    }

    public List<ClassEntry> GetBestSchedule(){
        List<ClassEntry> bestSchedule = new ArrayList<ClassEntry>();
        int bestCount = 0;
        List<List<ClassEntry>> allPossibleSchedules = GetAllSchedules();

        for(List<ClassEntry> entries : allPossibleSchedules){
            if(entries.size() > bestCount){
                bestCount = entries.size();
                bestSchedule = entries;
            }
        }

        return bestSchedule;
    }

    public List<List<ClassEntry>> GetAllSchedules(){
        List<List<ClassEntry>> allSchedules = new ArrayList<>();

        for(int i = 0; i < ClassPairs.size(); i++){
            if(i == 0){
                var sections = GetAllSections(ClassPairs.get(i).getName());
                for(ClassEntry entry : sections){
                    var startingList = new ArrayList<ClassEntry>();
                    startingList.add(entry);
                    allSchedules.add(startingList);
                }
            }else{
                List<List<ClassEntry>> nextAllSchedules = new ArrayList<>();
                for(List<ClassEntry> schedule : allSchedules){
                    for(ClassEntry entry : GetAllSections(ClassPairs.get(i).getName())){
                        var newSchedule = new ArrayList<ClassEntry>();
                        for(ClassEntry classE : schedule){
                            newSchedule.add(classE);
                        }
                        newSchedule.add(entry);
                        if(isValidSchedule(newSchedule)){
                            nextAllSchedules.add(newSchedule);
                        }
                    }
                }
            allSchedules = nextAllSchedules;
            }
        }


        return allSchedules;
    }

    private boolean isValidSchedule(List<ClassEntry> entries){

        for(ClassEntry entry : entries){
            for(ClassEntry entry1 : entries){
                if(entry == entry1)continue;
                if(entry.isClassEntryOverlap(entry1)){
                    return false;
                }
            }
        }

        return true;
    }
/*
    List<ClassEntry> GetScheduleRec(int index, List<ClassEntry> entries){

        for(int i = 0; i < index; i++){


        }

    }
*/
    List<ClassEntry> GetAllSections(String className){
        List<ClassEntry> matchingSections = new ArrayList<>();

        for(ClassEntry entry : ClassPool){
            if(entry.GetTitle().contentEquals(className)){
                matchingSections.add(entry);
            }
        }
        return matchingSections;
    }

}
