package window;

import DCTScheduleBuilder.CatalogQuery;
import DCTScheduleBuilder.ClassSchedule;
import DCTScheduleBuilder.TimeRange;
import javafx.beans.property.BooleanProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;

import java.util.*;

import DCTScheduleBuilder.ClassEntry;
import org.jsoup.Jsoup;


public class Controller {

    @FXML
    private CheckBox SummerCheckbox;

    @FXML
    private CheckBox FallCheckbox;

    @FXML
    private CheckBox SpringCheckbox;

    @FXML
    private ListView<ClassEntry> ClassSearchResultBox;

    @FXML
    private Button AddSelectedClassButton;

    @FXML
    private Button SearchID;

    @FXML
    private TextField SearchEntryBox;

    @FXML
    private Button SearchKeyword;

    @FXML
    private ListView<ClassPair> DesiredClassesListBox = new ListView<>();

    @FXML
    private Button PriorityUpButton;

    @FXML
    private Button PriorityDownButton;

    @FXML
    private Tab ScheduleTab;

    @FXML
    private ListView<ClassEntry> BestScheduleListBox;
    @FXML
    private TextField TeacherNameTextBox;

    @FXML
    private TextField StartDateTextBox;

    @FXML
    private TextField EndDateTextBox;

    @FXML
    private TextField CreditsTextBox;

    @FXML
    private ListView<TimeRange> ClassScheduleListBox;
    @FXML
    private TextField RoomTextBox;

    @FXML
    private TextField SectionTextBox;



    private ClassEntry selectedClassSearchEntry = null;
    private ClassPair desiredClassSearchEntry = null;
    private List<ClassPair> SelectedClassIds = new ArrayList<>();
    private List<ClassEntry> AllClasses = new ArrayList<>();

    @FXML
    void SpringClassesCheckboxClicked(MouseEvent event) {
        if(SpringCheckbox.isSelected()){
            SummerCheckbox.setSelected(false);
            FallCheckbox.setSelected(false);
        }
    }

    @FXML
    void SummerClassesCheckboxClicked(MouseEvent event) {
        if(SummerCheckbox.isSelected()){
            SpringCheckbox.setSelected(false);
            FallCheckbox.setSelected(false);
        }
    }

    @FXML
    void FallClassesCheckboxClicked(MouseEvent event){
        if(FallCheckbox.isSelected()){
            SummerCheckbox.setSelected(false);
            SpringCheckbox.setSelected(false);
        }
    }

    @FXML
    void AddSelectedClass(MouseEvent event) {

        if(selectedClassSearchEntry != null){
            if(!isClassAdded(selectedClassSearchEntry)){
                var pair = new ClassPair(selectedClassSearchEntry.GetTitle(),SelectedClassIds.size());
                System.out.println(pair);
                SelectedClassIds.add(pair);
                DesiredClassesListBox.setItems(FXCollections.observableList(SelectedClassIds));
            }
        }
    }

    @FXML
    void SearchClassesByID(MouseEvent event) {
        ArrayList<ClassEntry> classEntries = CatalogQuery.getInstance().GetClassesByID(SearchEntryBox.getText());
        AllClasses = mergeLists(filterEntries(classEntries,true ), AllClasses);

        classEntries = filterEntries(classEntries,false );

        ClassSearchResultBox.setItems(FXCollections.observableArrayList(classEntries));


    }

    private ArrayList<ClassEntry> filterEntries(ArrayList<ClassEntry> entries, boolean removeDuplicateClasses){
        ArrayList<ClassEntry> newEntries = new ArrayList<ClassEntry>();

        for(ClassEntry entry: entries){
            boolean existsIn = false;
            for(ClassEntry ne : newEntries){
                if(entry.GetTitle().contentEquals(ne.GetTitle()) && entry.GetSemester().contentEquals(ne.GetSemester()) ){
                    existsIn = true;
                }
            }
            if((!existsIn) | removeDuplicateClasses){
                if(SpringCheckbox.isSelected()){
                    if(entry.GetSemester().toLowerCase().contains("spring"))newEntries.add(entry);
                }else if(SummerCheckbox.isSelected()) {
                    if (entry.GetSemester().toLowerCase().contains("summer")) newEntries.add(entry);
                }else if(FallCheckbox.isSelected()) {
                    if (entry.GetSemester().toLowerCase().contains("fall")) newEntries.add(entry);
                }

            }
        }
    return newEntries;
    }

    private boolean isClassAdded(ClassEntry entry){
        boolean existsInList = false;
        for(ClassPair classPair : SelectedClassIds){
            if(classPair.getName().contentEquals(entry.GetTitle())){
                existsInList = true;
            }
        }

        return existsInList;
    }

    private List<ClassEntry> mergeLists(List<ClassEntry> L1,List<ClassEntry> L2){
       List<ClassEntry> newList = new ArrayList<ClassEntry>();

            for(ClassEntry entry : L2){
                boolean containedIn = false;
                for(ClassEntry otherEntry : L1){
                    if(entry.hashCode() == otherEntry.hashCode())
                    {
                        containedIn = true;
                    }
                }
                if(!containedIn){
                    newList.add(entry);
                }
            }

            for(ClassEntry entry : L1){
                newList.add(entry);
            }

        return newList;
    }

    @FXML
    void SearchClassesByKeyword(MouseEvent event) {

    }

    @FXML
    void ClassSearchEntryClicked(MouseEvent event){
        ListView lv = (ListView)event.getSource();

        ClassEntry selectedItem = (ClassEntry)lv.getSelectionModel().getSelectedItem();

        if (selectedItem != null){
            selectedClassSearchEntry = selectedItem;
            System.out.println("Selected item: " + selectedItem.toString());
        }else{
            System.out.println("Invalid selection");
        }


    }

    @FXML
    void DesiredClassesListViewClicked(MouseEvent event) {
        ListView lv = (ListView)event.getSource();

        ClassPair selectedItem = (ClassPair)lv.getSelectionModel().getSelectedItem();

        if (selectedItem != null){
            desiredClassSearchEntry = selectedItem;
            System.out.println("Selected item: " + selectedItem.toString());
        }else{
            System.out.println("Invalid selection");
        }


    }
    void swapPriorities(ClassPair p1, ClassPair p2){
        int p1P = p1.getPriority();
        p1.SetPriority(p2.getPriority());
        p2.SetPriority(p1P);
    }

    @FXML
    void DownPriorityClicked(MouseEvent event) {
        var pairs = DesiredClassesListBox.getItems();
        List<ClassPair> newPairs = new ArrayList<ClassPair>();

        if(desiredClassSearchEntry.getPriority() == pairs.size()-1){
            return;
        }

        swapPriorities(pairs.get(desiredClassSearchEntry.getPriority()+1), desiredClassSearchEntry);

        for(int i = 0; i < pairs.size(); i++){
            for(ClassPair pair : pairs){
                if(pair.getPriority() == i){
                    newPairs.add(pair);
                }
            }
        }

        DesiredClassesListBox.setItems(FXCollections.observableList(newPairs));
    }

    @FXML
    void UpPriorityClicked(MouseEvent event) {
        var pairs = DesiredClassesListBox.getItems();
        List<ClassPair> newPairs = new ArrayList<ClassPair>();

        if(desiredClassSearchEntry.getPriority() == 0){
            return;
        }

        swapPriorities(pairs.get(desiredClassSearchEntry.getPriority()-1), desiredClassSearchEntry);

        for(int i = 0; i < pairs.size(); i++){
            for(ClassPair pair : pairs){
                if(pair.getPriority() == i){
                    newPairs.add(pair);
                }
            }
        }

        DesiredClassesListBox.setItems(FXCollections.observableList(newPairs));
    }


    @FXML
    void GetBestScheduleClicked(MouseEvent event) {
    //BestSchedule
        ArrayList<ClassEntry> classPool = new ArrayList<ClassEntry>();
        var pairs = DesiredClassesListBox.getItems();

        for(ClassEntry entry : AllClasses){
            boolean selectedClass = false;
            for(ClassPair selectedEntry : pairs){
                if(entry.GetTitle().contentEquals(selectedEntry.getName())){
                    selectedClass = true;
                }
            }
            if(selectedClass){
                classPool.add(entry);
            }
        }
        ScheduleCreator sc = new ScheduleCreator(pairs, classPool);
        List<ClassEntry> bestSchedule = sc.GetBestSchedule();

        if(bestSchedule != null){
            BestScheduleListBox.setItems(FXCollections.observableArrayList(bestSchedule));
        }else{
            System.out.println("bestSchedule is null!@");
        }



    //classPool.forEach(e->System.out.println(e.toString() + "\n" + e.GetClassSection().toString()));


    }

    @FXML
    void ClassClickedInSchedule(MouseEvent event) {
        //ClassScheduleListBox
        //   private TextField TeacherNameTextBox;
        //    private TextField StartDateTextBox;
        //    private TextField EndDateTextBox;
        //    private TextField CreditsTextBox;

        ListView lv = (ListView)event.getSource();
        ClassEntry entry = (ClassEntry)lv.getSelectionModel().getSelectedItem();
        if(entry != null){
            ClassScheduleListBox.setItems(FXCollections.observableList(entry.GetClassSection().GetClassSchedule().GetClassSessions()));
            TeacherNameTextBox.setText(entry.GetClassSection().GetTeacherName());
            StartDateTextBox.setText(entry.GetClassSection().GetClassStartDate());
            EndDateTextBox.setText(entry.GetClassSection().GetClassEndDate());
            CreditsTextBox.setText(entry.GetClassCredits());
            RoomTextBox.setText(entry.GetClassSection().GetClassRoom());
            SectionTextBox.setText(entry.GetClassSection().GetSectionID());
        }
    }



}
