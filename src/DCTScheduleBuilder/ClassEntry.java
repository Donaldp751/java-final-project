package DCTScheduleBuilder;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class ClassEntry {
    private String ClassTitle;
    private String ClassID;
    private ClassSection ClassSection;
    private String ClassDateRange;
    private String ClassSemester;
    private String ClassCredits;


    public ClassEntry(){

    }

    public boolean isClassEntryOverlap(ClassEntry entry){
        ClassSchedule cs = ClassSection.GetClassSchedule();
        return cs.isOverlapped(entry.GetClassSection().GetClassSchedule());
    }
    public ClassEntry SetTitle(String title){
        ClassTitle = title;
        return this;
    }
    public ClassEntry SetClassID(String iD){
        ClassID = iD;
        return this;
    }
    public ClassEntry SetClassDateRange(String dateRange){
        ClassDateRange = dateRange;
        return this;
    }
    public ClassEntry SetClassCredits(String credits){
        ClassCredits = credits;
        return this;
    }
    public ClassEntry SetClassSection(ClassSection section){
        ClassSection = section;
        return this;
    }
    public ClassEntry SetClassSemester(String classSemester){
        ClassSemester = classSemester;
        return this;
    }

    public ClassSection GetClassSection(){
        return ClassSection;
    }
    public String GetSemester(){
        return ClassSemester;
    }
    public String GetTitle(){
        return ClassTitle;
    }
    public String GetClassID(){
        return ClassID;
    }
    public String GetClassDateRange(){
        return ClassDateRange;
    }
    public String GetClassCredits(){
        return ClassCredits;
    }

    @Override
    public String toString(){
        return ClassTitle + " : " + ClassID + " : " + GetSemester() + " : " + ClassCredits;
    }
    @Override
    public int hashCode(){
        return Objects.hash(ClassTitle, ClassID, ClassCredits);
    }
}
