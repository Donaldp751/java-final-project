package DCTScheduleBuilder;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ClassSchedule {
    private List<TimeRange> ClassSessions;

    public ClassSchedule(){
        ClassSessions = new ArrayList<TimeRange>();
    }
    public void AddClassTimeRange(TimeRange day){
        ClassSessions.add(day);
    }
    public List<TimeRange> GetClassSessions(){
        return ClassSessions;
    }
    public boolean isOverlapped(ClassSchedule cs){
        List<TimeRange> tr = cs.GetClassSessions();

        for(TimeRange tr1 : ClassSessions){
            for(TimeRange tr2 : tr){
                if(tr1.isOverlapped(tr2)){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String toString(){
        String buf = "\n";
        for(TimeRange tr : GetClassSessions()){
            buf = tr.toString() + buf;
        }
        return buf;
    }

}
