package DCTScheduleBuilder;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.Month;
import java.time.MonthDay;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class CatalogQuery {
    private static CatalogQuery ourInstance = new CatalogQuery();
    public static CatalogQuery getInstance() {
        return ourInstance;
    }

    final String ClassesCount = "&num=500"; //Always show all of the classes
    final String myDunwoodyURL = "https://my.dunwoody.edu/my/Search/SectionSearch.aspx?";
    final String URLKeywordSearchKeyword = "criteria="; //append the keyword to be searched
    final String URLIDSearchKeyword = "eventid="; //append the course id to be searched

    public CatalogQuery() {

    }

    public ArrayList<ClassEntry> GetClassesByID(String ClassID){
        Document doc = GetCatalogDocument(URLIDSearchKeyword + ClassID);

        Elements classElements = GetDocumentClassElements(doc);
        System.out.println(classElements.size());

        ArrayList<ClassEntry> entries = new ArrayList<>();

        List<String> test = new ArrayList<>();


        for(Element e : classElements){


            Elements EntryParts = e.select("td");

            if(EntryParts.size() <4){
                continue;
            }

            int index = classElements.indexOf(e)+1;
            String teacherName = classElements.get(index).select("td").get(1).text();


            String classNameID = EntryParts.get(1).text();

            String slashSplit[] = classNameID.split("/");

            String spaceSplit[] = slashSplit[0].split(" ");

            String classTitle = "";
            for(int i = 0; i < spaceSplit.length-1; i++){
                classTitle += spaceSplit[i] + " ";
            }

            String classID = spaceSplit[spaceSplit.length-1];


            String classDateRangeText = EntryParts.get(2).text();
            String classScheduleRoomText = EntryParts.get(6).text();
            String classSizeText = EntryParts.get(7).text();
            String classSemesterText = EntryParts.get(3).text();

            String startDateText = classDateRangeText.split("-")[0];
            String endDateText = classDateRangeText.split("-")[1];

            ClassSection cs = new ClassSection();
            cs.SetTeacherName(teacherName);
            cs.SetSectionID(slashSplit[slashSplit.length-1]);
            //cs.SetClassStartDate(new Date());
            Date StartDate = new Date(Integer.parseInt(startDateText.split("/")[2].strip())-1900,Integer.parseInt(startDateText.split("/")[0].strip()),Integer.parseInt(startDateText.split("/")[1].strip()));
            Date EndDate = new Date(Integer.parseInt(endDateText.split("/")[2].strip())-1900,Integer.parseInt(endDateText.split("/")[0].strip()),Integer.parseInt(endDateText.split("/")[1].strip()));
            cs.SetClassStartDate(StartDate);
            cs.SetClassEndDate(EndDate);

            cs.SetCurrentRegisteredClassSize(Integer.parseInt(classSizeText.split(" ")[0]));
            cs.SetMaxClassSize(Integer.parseInt(classSizeText.split(" ")[2]));

            String classScheduleText = classScheduleRoomText.split(";")[0];
            String classRoomText = classScheduleRoomText.split(";")[1].split(",")[2].strip();
            classRoomText = classRoomText.split(" ")[1];


            ClassSchedule classSchedule = new ClassSchedule();

            if(classScheduleText.contains("/")){
                String[] splitDays = classScheduleText.split(" ");

                String days = splitDays[0];


                String timeString = "";
                for(int i = 1; i < splitDays.length; i++){
                    timeString += splitDays[i] + " ";
                }

                String[] splitTime = timeString.split("-");

                var startingTime = splitTime[0].strip();
                var endingTime = splitTime[1].strip();

                LocalTime startingLocalTime = parseLocalTime(startingTime);
                LocalTime endingLocalTime = parseLocalTime(endingTime);

                String[] daysSplit = days.split("/");

                for(int i = 0; i < daysSplit.length; i++){
                    TimeRange timeRange = new TimeRange(startingLocalTime, endingLocalTime, DayOfWeek.of(GetDayNumberFromString(daysSplit[i])));
                    classSchedule.AddClassTimeRange(timeRange);
                }

            }else if(classScheduleText.contains("thru")){
                String[] splitDays = classScheduleText.split(" ");
                String firstDay = splitDays[0];
                String lastDay = splitDays[2];
                int classesAWeek = GetDayNumberFromString(lastDay) - GetDayNumberFromString(firstDay);

                String timeString = "";
                for(int i = 3; i < splitDays.length; i++){
                    timeString += splitDays[i] + " ";
                }

                String[] splitTime = timeString.split("-");

                var startingTime = splitTime[0].strip();
                var endingTime = splitTime[1].strip();

                LocalTime startingLocalTime = parseLocalTime(startingTime);
                LocalTime endingLocalTime = parseLocalTime(endingTime);



                for(int i = 0; i <= classesAWeek; i++){
                    TimeRange timeRange = new TimeRange(startingLocalTime, endingLocalTime, DayOfWeek.of(i+GetDayNumberFromString(firstDay)));
                    classSchedule.AddClassTimeRange(timeRange);
                }

            }else{
                int classDay = 0;

                String timeString = "";
                var split = classScheduleText.split(" ");
                for(int i = 0; i < split.length; i++){
                    if(i == 0){
                        classDay = GetDayNumberFromString(split[0].toLowerCase());
                    }
                    else{
                        timeString += split[i]+" ";
                    }
                }

                split = timeString.split("-");

                var startingTime = split[0].strip();
                var endingTime = split[1].strip();

                LocalTime startingLocalTime = parseLocalTime(startingTime);
                LocalTime endingLocalTime = parseLocalTime(endingTime);

                TimeRange classTimeRange = new TimeRange(startingLocalTime, endingLocalTime, DayOfWeek.of(classDay));
                classSchedule.AddClassTimeRange(classTimeRange);

            }

            cs.SetClassSchedule(classSchedule);
            cs.SetClassRoom(classRoomText);
            test.add(classScheduleText);

            ClassEntry entry = new ClassEntry()
                    .SetTitle(classTitle)
                    .SetClassID(classID)
                    .SetClassDateRange(EntryParts.get(2).text())
                    .SetClassCredits(EntryParts.get(4).text())
                    .SetClassSemester(classSemesterText)
                    .SetClassSection(cs);



            entries.add(entry);

            System.out.println();
            System.out.println(e.text());

        }

        test.forEach(s -> System.out.println(s));

        return entries;
    }


    private LocalTime parseLocalTime(String time){
        LocalTime localTime = null;

        if(time.contains("PM")){
            time = FormatTimeForParsing(time);

            localTime = LocalTime.parse(time).plusHours(12);

        }else{
            time = FormatTimeForParsing(time);
        }

        localTime = LocalTime.parse(time);
    return localTime;
    }

    private String FormatTimeForParsing(String time){
        time = time.split(" ")[0];

        if(time.split(":")[0].length() == 1){
            time = "0"+time;
        }

        if(time.split(":")[1].length() == 1){
            time = time.split(":")[0] + ":" + "0" + time.split(":")[1];
        }
        return time;
    }

    private int GetDayNumberFromString(String dayName){
        for(var day : DayOfWeek.values()){
            var dayString = day.toString().toLowerCase();
            if(dayString.contains(dayName.toLowerCase())){
                return day.getValue();
            }
        }
        return -1;
    }

    public ArrayList<String> GetClassesByKeyword(String ClassKeyword){

        return new ArrayList<String>();
    }

    private ArrayList<ClassEntry> ConvertToClasses(){

        return new ArrayList<ClassEntry>();
    }

    private Elements GetDocumentClassElements(Document document){

        try{

            Elements elements = document.select(".defaultTable");
            elements = elements.select("tr");

            //for(Element element: elements){
            //    System.out.println(element.text());
            //}

            return elements;

        }catch(Exception e) {
            System.out.println("Exception thrown in GetDocumentClassElements");
            System.out.println(e.toString());
        }
        return null;
    }

    private Document GetCatalogDocument(String URLSpecificAttr){
        try {
            String URL = myDunwoodyURL + URLSpecificAttr + ClassesCount;
            System.out.println("URL: " + URL);
            //System.out.println("URL Specific: " + URLSpecificAttr);
            Connection connection = Jsoup.connect(URL);
            System.out.println("Connection made successfully");

            Document document = connection.get();
            System.out.println("Document get successfully");
            return document;
            //searchPage = connection.get();

        }catch(Exception e){
            System.out.println("Exception thrown in GetCatalogDocument");
            System.out.print(e.toString());
        }


        return null;
    }
}
