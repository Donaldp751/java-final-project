package DCTScheduleBuilder;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Date;
import java.util.Objects;

public class ClassSection {
    private ClassSchedule ClassSchedule;
    private Date ClassStartDate;
    private Date ClassEndDate;
    private int MaxClassSize;
    private int CurrentRegisteredClassSize;
    private String TeacherName;
    private String ClassRoom;
    private String SectionID;
    private String pattern = "MM/dd/yyyy";



    public ClassSection(){
    }

    public void SetClassStartDate(Date startDate){
        ClassStartDate = startDate;
    }
    public void SetClassEndDate(Date endDate){
        ClassEndDate = endDate;
    }
    public void SetSectionID(String id){
        SectionID = id;
    }
    public void SetCurrentRegisteredClassSize(int size){
        CurrentRegisteredClassSize = size;
    }
    public void SetClassSchedule(ClassSchedule classSchedule){
        ClassSchedule = classSchedule;
    }

    public void SetMaxClassSize(int size){
        MaxClassSize = size;
    }

    public ClassSchedule GetClassSchedule(){
        return ClassSchedule;
    }
    public String GetSectionID(){
        return SectionID;
    }
    public void SetTeacherName(String teacherName){
        TeacherName = teacherName;
    }
    public void SetClassRoom(String classRoom){
        ClassRoom = classRoom;
    }

    public String GetClassStartDate(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(ClassStartDate);
        return date;
    }
    public String GetClassEndDate(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(ClassEndDate);
        return date;
    }

    public String GetTeacherName(){
        return TeacherName;
    }
    public String GetClassRoom(){
        return ClassRoom;
    }

    @Override
    public int hashCode(){
        return Objects.hash(ClassStartDate, ClassEndDate, TeacherName, ClassRoom, SectionID);
    }
    @Override public String toString(){return SectionID + "\n" + ClassSchedule.toString();}
}
