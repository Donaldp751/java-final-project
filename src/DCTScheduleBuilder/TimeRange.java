package DCTScheduleBuilder;


import java.time.DayOfWeek;
import java.time.LocalTime;

public class TimeRange {
        LocalTime StartTime;
        LocalTime EndTime;
        DayOfWeek Day;

        public TimeRange(LocalTime startTime, LocalTime endTime, DayOfWeek classDay){
                StartTime = startTime;
                EndTime = endTime;
                Day = classDay;
        }
        public boolean isOverlapped(TimeRange tr){
                
                if(Day != tr.Day){
                        //System.out.println("Days are not the same");
                        return false;
                }else{
                        if(StartTime.compareTo(tr.StartTime) <= 0 && EndTime.compareTo(tr.StartTime) >= 0){
                                //System.out.println("Overlaps by case 1");
                                return true;
                        }else if(StartTime.compareTo(tr.EndTime) <= 0 && EndTime.compareTo(tr.StartTime) >= 0){
                                //System.out.println("Overlaps by case 2");
                                return true;
                        }else if(StartTime.compareTo(tr.StartTime) >= 0 && EndTime.compareTo(tr.EndTime) <= 0){
                                //System.out.println("Overlaps by case 3");
                                return true;
                        }
                return false;
                }

        }
        @Override
        public String toString(){
                return Day.toString()+ " : " + StartTime.toString() + " : " + EndTime.toString() + "\n";
        }
}
